package ru.t1.artamonov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(length = 50)
    private String login;

    @Nullable
    @Column(name = "pswrd_hash", length = 200)
    private String passwordHash;

    @Nullable
    @Column(length = 200)
    private String email;

    @Nullable
    @Column(name = "fst_name", length = 100)
    private String firstName;

    @Nullable
    @Column(name = "lst_name", length = 100)
    private String lastName;

    @Nullable
    @Column(name = "mdl_name", length = 100)
    private String middleName;

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "lock_flg")
    private Boolean locked = false;

}
