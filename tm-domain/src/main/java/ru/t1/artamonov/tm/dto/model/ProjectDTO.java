package ru.t1.artamonov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.model.IWBS;
import ru.t1.artamonov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(length = 100)
    private String name = "";

    @NotNull
    @Column(name = "descrptn", length = 2000)
    private String description = "";

    @NotNull
    @Column(length = 50)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDTO(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(@NotNull final String name, @NotNull final String description, @NotNull final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
