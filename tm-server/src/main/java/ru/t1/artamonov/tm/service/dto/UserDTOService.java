package ru.t1.artamonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.api.service.dto.IUserDTOService;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.EmailEmptyException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.LoginEmptyException;
import ru.t1.artamonov.tm.exception.field.PasswordEmptyException;
import ru.t1.artamonov.tm.exception.user.ExistsEmailException;
import ru.t1.artamonov.tm.exception.user.ExistsLoginException;
import ru.t1.artamonov.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class UserDTOService implements IUserDTOService {

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO add(@Nullable UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setRole(Role.USUAL);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setRole(Role.USUAL);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findOneById(id);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isLoginExist(@Nullable String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull UserDTO remove(@Nullable UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        return removeById(user.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull UserDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> users) {
        if (users == null) return new ArrayList<>();
        for (@NotNull UserDTO user : users)
            userRepository.add(user);
        return users;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(@Nullable String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        userRepository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.update(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.update(user);
    }

}
