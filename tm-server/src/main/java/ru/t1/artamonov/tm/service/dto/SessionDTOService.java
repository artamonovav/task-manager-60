package ru.t1.artamonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.artamonov.tm.api.service.dto.ISessionDTOService;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.exception.entity.ModelNotFoundException;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.exception.user.AccessDeniedException;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public class SessionDTOService implements ISessionDTOService {

    @NotNull
    @Autowired
    private ISessionDTORepository sessionRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO add(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.add(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new AccessDeniedException();
        model.setUserId(userId);
        sessionRepository.add(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull List<SessionDTO> add(@NotNull List<SessionDTO> models) {
        if (models == null) throw new ModelNotFoundException();
        for (@NotNull SessionDTO model : models) {
            sessionRepository.add(model);
        }
        return models;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable List<SessionDTO> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findOneById(id);
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findOneByIdUserId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO update(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.update(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO remove(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.remove(session);
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull SessionDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(userId, id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.remove(session);
        return session;
    }

}
