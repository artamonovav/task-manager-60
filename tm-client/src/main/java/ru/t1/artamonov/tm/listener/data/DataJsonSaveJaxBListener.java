package ru.t1.artamonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataJsonSaveJaxBRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveJaxBListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data in json file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE JSON]");
        @NotNull DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(getToken());
        domainEndpointClient.saveDataJsonJaxB(request);
    }

}
