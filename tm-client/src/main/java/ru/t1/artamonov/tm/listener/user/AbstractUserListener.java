package ru.t1.artamonov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.artamonov.tm.api.endpoint.IUserEndpoint;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpointClient;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
