package ru.t1.artamonov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.IListener;
import ru.t1.artamonov.tm.api.service.IServiceLocator;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements IListener {

    @Nullable
    @Autowired
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String getName();

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description + "\n";
        return result;
    }

}
